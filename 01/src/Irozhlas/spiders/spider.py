import scrapy
import unicodedata


class IrozhlasSpider(scrapy.Spider):
    name = 'irozhlasspider'
    start_urls = ['https://www.irozhlas.cz/veda-technologie?page=0']

    def parse(self, response, **kwargs):
        for article in response.css('div.col.col--main div.c-articles__list article.c-articles__item'):
            yield {
                'title': self.get_title(article),
                'published at': self.get_published_at(article),
                'author': self.get_author(article),
                'content': self.get_content(article)}

        next_page = response.css(
            'nav.m-pagination a.m-pagination__item.m-pagination__item--next ::attr(href)').extract_first()
        if next_page:
            yield scrapy.Request(response.urljoin(next_page), callback=self.parse)

    @staticmethod
    def get_title(article):
        return unicodedata.normalize("NFKD", article.css('a.b-article__link::text').extract_first().strip())

    @staticmethod
    def get_published_at(article):
        return article.css('div.b-article__content time::text').extract_first()

    @staticmethod
    def get_author(article):
        author = article.css('div.b-article__content span.hide--m a.meta__link::text').extract_first()
        return author if author is not None else "Unknown"

    @staticmethod
    def get_content(article):
        return article.css('p.text-sm::text').extract_first().strip()
