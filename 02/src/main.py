import nltk
import wikipedia
from pprint import pprint


# Load data
def get_data():
    with open('FOTR.txt', 'r', encoding="utf8") as f:
        text = f.read()
    return text


# POS tagging
def get_POS(text):
    sentences = nltk.sent_tokenize(text)
    tokens = [nltk.word_tokenize(sent) for sent in sentences]
    tagged = [nltk.pos_tag(sent) for sent in tokens]
    return tagged


# NER with entity classification (using nltk.ne_chunk)
def get_NER(text):
    tokens = nltk.word_tokenize(text)
    tagged = nltk.pos_tag(tokens)
    ne_chunked = nltk.ne_chunk(tagged)
    data = extract_entities(ne_chunked)

    return data


# Extract entities from ne_chunked with their counts, sort them
def extract_entities(ne_chunked):
    data = {}
    for entity in ne_chunked:
        if isinstance(entity, nltk.tree.Tree):
            text = " ".join([word for word, tag in entity.leaves()])
            ent = entity.label()
            if text not in data:
                data[text] = [ent, 0]
            data[text][1] += 1
        else:
            continue
    sorted_data = sorted(data.items(), key=lambda entity: entity[1][1], reverse=True)
    return sorted_data


def get_custom_NER(text):
    tokens = nltk.word_tokenize(text)
    tagged = nltk.pos_tag(tokens)

    data = {}
    entity = []
    for tagged_entry in tagged:
        # all adjectives and optionally plus noun
        if tagged_entry[1].startswith("JJ") or (entity and tagged_entry[1].startswith("NN")):
            entity.append(tagged_entry)
        else:
            if entity:
                # get rid of not real adjectives
                if len(entity[0][0]) <= 2:
                    entity = []
                else:
                    word = " ".join(e[0] for e in entity)
                    if word not in data:
                        data[word] = [entity[0][1], 0]
                    data[word][1] += 1
                    entity = []
    sorted_data = sorted(data.items(), key=lambda entity: entity[1][1], reverse=True)
    return sorted_data


def categorize_by_wikipedia(entity: str):
    # Get first sentence from wikipedia summary and tag it
    summary_sentence = nltk.pos_tag(nltk.word_tokenize(wikipedia.summary(entity[0], sentences=1, auto_suggest=False)))

    # Detect pattern of describing what given entity is
    grammar = "NP: {<DT>?<JJ>?<VBN>*<NN>?<NN>}"
    cp = nltk.RegexpParser(grammar)
    chunked = cp.parse(summary_sentence)

    for word in chunked:
        # Skip first (Repeats entity name)
        if word == chunked[0] or len(word) < 2:
            continue
        if isinstance(word, nltk.tree.Tree):
            text = " ".join([word for word, tag in word.leaves()])
            return text
        else:
            continue
    return "Thing"


def print_wiki_categorization(entity, category):
    print("{} ------> {}".format(entity, category))


def main():
    limit_results = 40

    text = get_data()
    tagged_tokens = get_POS(text)
    print("=" * 80)
    print("Tokens (truncated):")
    pprint(tagged_tokens[:limit_results], indent=2)
    print("=" * 80)

    named_entities = get_NER(text)
    print("=" * 80)
    print("Top recognized entities:")
    pprint(named_entities[:limit_results])

    custom_parsed_entities = get_custom_NER(text)
    print("=" * 80)
    print("Top custom tagged entities:")
    pprint(custom_parsed_entities[:limit_results])

    # NER result wiki classification
    print("=" * 80)
    print("Tagged NLTK entities through Wikipedia:")
    count = 0
    for named_entity in named_entities:
        print_wiki_categorization(named_entity, categorize_by_wikipedia(named_entity))
        count += 1
        if count > limit_results:
            break

    # custom NER result wiki classification
    print("=" * 80)
    print("Tagged custom entities through Wikipedia:")
    count = 0
    for named_entity in custom_parsed_entities:
        print_wiki_categorization(named_entity, categorize_by_wikipedia(named_entity))
        count += 1
        if count > limit_results:
            break


if __name__ == '__main__':
    main()
