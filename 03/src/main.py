import csv
import itertools

import networkx
from networkx.algorithms.community import k_clique_communities

INPUT_LIMIT = 8000


class Cast:
    def __init__(self, csv_row):
        self.movie_id = csv_row[0]
        self.movie_title = csv_row[1]
        self.actor_name = csv_row[2]
        self.role_type = csv_row[3]
        roles = csv_row[4].split(':', 1)
        role_prefix = roles[0]
        try:
            role_content = roles[1]
        except IndexError:
            role_content = ""
        self.role_prefix = role_prefix
        self.role_content = role_content


def load_data(fn):
    data = []

    with open(fn) as csv_file:
        csv_reader = itertools.islice(csv.reader(csv_file, delimiter=';'), INPUT_LIMIT)
        [data.append(row) for row in csv_reader]

    return data


def make_graph(records):
    movie_actors = {}  # Mapping {movie: [act1, act2]}
    g = networkx.Graph()
    for record in records:
        g.add_node(record.actor_name)
        if record.movie_title not in movie_actors:
            movie_actors[record.movie_title] = []
        movie_actors[record.movie_title].append(record.actor_name)

    for movie, actor_list in movie_actors.items():
        [g.add_edge(actor_match[0], actor_match[1]) for actor_match in list(itertools.combinations(actor_list, 2))]

    return g


def print_stats(graph):
    nodes = graph.number_of_nodes()
    edges = graph.number_of_edges()
    density = networkx.density(graph)
    connected_components = networkx.number_connected_components(graph)

    print("=" * 80)
    print("""BASIC STATISTICS:
            Number of nodes: {nodescnt}
            Number of edges: {edgescnt}
            Density: {density}
            Number of components: {components}"""
          .format(nodescnt=nodes, edgescnt=edges, density=density, components=connected_components))
    print("=" * 80)


def print_communities(graph):
    communities = {node: cid + 1 for cid, community in enumerate(k_clique_communities(graph, 3))
                   for node in community}

    # Group actors from same communities together
    actor_communities = {}
    for key, val in communities.items():
        if val not in actor_communities:
            actor_communities[val] = []
        actor_communities[val].append(key)

    # Sort based on the length of the list of actors
    sorted_actor_communities = sorted(actor_communities.items(), key=lambda element: len(element[1]), reverse=True)

    print("=" * 80)
    print("12 biggest actor communities:")
    for community in sorted_actor_communities[:12]:
        print("ID {}, {} actors: {}".format(community[0], len(community[1]), ", ".join(community[1])))
    print("=" * 80)

    # Add as attribute to graph
    for actor, community_id in communities.items():
        graph.node[actor]['community_id'] = community_id


def print_centralities(graph):
    centralities = [
        networkx.degree_centrality,
        networkx.closeness_centrality,
        networkx.betweenness_centrality,
    ]

    print("=" * 80)
    print("CENTRALITIES:")
    for centrality in centralities:
        centrality_res = centrality(graph)

        # Add as node attribute
        for actor, centrality_val in centrality_res.items():
            graph.node[actor][centrality.__name__] = centrality_val

        centrality_res_sorted = sorted(centrality_res.items(), key=lambda element: element[1], reverse=True)
        print("{} - top 10: ".format(centrality.__name__))
        print("  {}".format(", ".join(["{} ({})".format(elm[0], elm[1]) for elm in centrality_res_sorted[:10]])))

    print("=" * 80)


def describe_kevin_bacon(graph, person):
    lengths = networkx.single_source_shortest_path_length(graph, person)

    # Add as node attribute
    for actor in graph.nodes():
        graph.node[actor]['KevinBaconLen'] = -1

    length_sum = 0
    length_count = 0
    for actor, length in lengths.items():
        graph.node[actor]['KevinBaconLen'] = length
        length_sum += length

        length_count += 1

    bacon_average = length_sum / length_count

    lengths_sorted = sorted(lengths.items(), key=lambda element: element[1], reverse=True)
    print("=" * 80)
    print("KevBacon! From person: {}".format(person))
    print("Average: {}".format(bacon_average))
    print("Shortest 10: {}".format(
        ", ".join(["{} ({})".format(actor_len[0], actor_len[1]) for actor_len in lengths_sorted[-10:]])))
    print("Highest 10:  {}".format(
        ", ".join(["{} ({})".format(actor_len[0], actor_len[1]) for actor_len in lengths_sorted[:10]])))
    print("=" * 80)


def main():
    data = load_data('casts.csv')
    records = []
    # Take only nonempty actors and not wrong inputs "s a..."
    [records.append(Cast(row)) if (row[2] != "" and not row[2].startswith("s a")) else {} for row in data]

    print("Loaded casts.csv: {}".format(len(records)))

    graph = make_graph(records)

    print_stats(graph)
    print_communities(graph)
    print_centralities(graph)
    describe_kevin_bacon(graph, 'Peter Lorre')

    networkx.write_gexf(graph, 'output_exported_graph.gexf')


if __name__ == '__main__':
    main()
