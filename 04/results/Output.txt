-----------CLICKS-------------
             LocalID        PageID  ...     PageScore  SequenceNumber
count   38451.000000  38451.000000  ...  38451.000000    38451.000000
mean   667685.000000   3286.412551  ...    143.092975        3.555122
std     11099.991937    408.577090  ...    260.595877        4.269960
min    648460.000000   3044.000000  ...     30.000000        1.000000
25%    658072.500000   3047.000000  ...     30.000000        1.000000
50%    667685.000000   3093.000000  ...     62.000000        2.000000
75%    677297.500000   3317.000000  ...    125.000000        4.000000
max    686910.000000   5258.000000  ...   5753.000000       50.000000

[8 rows x 9 columns]
-----------VISITORS-------------
            VisitID          Hour  Length_seconds  Length_pagecount
count  15559.000000  15559.000000    15559.000000      15559.000000
mean    9225.450157     13.814705      128.908028          2.471239
std     4654.823196      4.809969      328.777507          2.998959
min     1185.000000      0.000000        0.000000          1.000000
25%     5181.500000     11.000000        0.000000          1.000000
50%     9223.000000     14.000000        0.000000          1.000000
75%    13266.500000     17.000000      120.000000          3.000000
max    17265.000000     23.000000     5280.000000         50.000000
-----------SEARCH ENGINE MAP-------------
       Referrer   Type
count       140    134
unique      140      5
top      URI_14  Other
freq          1     73
-----------NO CLEANING SIZE-------------
692118
-----------SIZE AFTER REMOVING TOO SHORT-------------
486738
-----------MAIN CONVERSIONS-------------
       PageID  VisitID     PageName  ... Length_seconds  Length_pagecount      Type
27       3065     1189      CATALOG  ...          540.0               7.0       NaN
49       3065     1200      CATALOG  ...           60.0               2.0  Fulltext
67       3065     1207      CATALOG  ...          120.0               4.0  Partners
111      3065     1226      CATALOG  ...           60.0               2.0       NaN
175      3118     1259  APPLICATION  ...          300.0               7.0  Fulltext
...       ...      ...          ...  ...            ...               ...       ...
38038    3065    17095      CATALOG  ...         1860.0               7.0  Fulltext
38053    3065    17101      CATALOG  ...           60.0               3.0  Partners
38090    3065    17113      CATALOG  ...         1320.0              13.0  Partners
38100    3065    17116      CATALOG  ...          480.0               9.0  Fulltext
38398    3065    17243      CATALOG  ...          240.0               5.0       NaN

[497 rows x 18 columns]
-----------MICRO CONVERSIONS-------------
       PageID  VisitID   PageName  ... Length_seconds  Length_pagecount       Type
37       3066     1192   DISCOUNT  ...          300.0               8.0  Catalogue
38       3066     1192   DISCOUNT  ...          300.0               8.0  Catalogue
56       3075     1204   WHOWEARE  ...          120.0               6.0   Fulltext
92       3097     1216  HOWTOJOIN  ...         1200.0              11.0   Fulltext
94       3099     1216   DISCOUNT  ...         1200.0              11.0   Fulltext
...       ...      ...        ...  ...            ...               ...        ...
37688    3075    16943   WHOWEARE  ...          420.0               6.0        NaN
37821    3066    17007   DISCOUNT  ...          780.0               9.0    OwnWebs
37843    3066    17014   DISCOUNT  ...           60.0               2.0        NaN
38217    3075    17162   WHOWEARE  ...           60.0               4.0  Catalogue
38397    3066    17243   DISCOUNT  ...          240.0               5.0        NaN

[442 rows x 18 columns]
-----------ALL VISITED PAGES BY USER-------------
      PageID_x  VisitID  ... PageScore_y SequenceNumber_y
0         3065     1189  ...          60                1
1         3065     1189  ...         406                2
2         3065     1189  ...         286                4
3         3065     1189  ...         125                3
4         3065     1189  ...          78                5
...        ...      ...  ...         ...              ...
3047      3065    17243  ...          60                1
3048      3065    17243  ...         101                2
3049      3065    17243  ...          78                5
3050      3065    17243  ...         125                3
3051      3065    17243  ...         143                4

[3052 rows x 29 columns]
-----------APRIORI FROM MAIN CONVERSIONS RESULTS-------------
frozenset({'sightseeing tours'}) - 0.14893617021276595
frozenset({'Corsica'}) - 0.09361702127659574
frozenset({'Alps tourism'}) - 0.08723404255319149
frozenset({'cycling abroad'}) - 0.09148936170212765
frozenset({'expedition'}) - 0.08085106382978724
frozenset({'tours and holiday comes into hotel'}) - 0.17659574468085107
frozenset({'Aeolian Islands'}) - 0.08085106382978724
frozenset({'light hiking'}) - 0.14680851063829786
frozenset({'lastminute'}) - 0.23829787234042554
frozenset({'TravelAgency'}) - 1.0659574468085107
frozenset({'tours with tents'}) - 0.17872340425531916
frozenset({'hiking'}) - 0.17872340425531916
frozenset({'WHOWEARE'}) - 0.1425531914893617
frozenset({'CATALOG'}) - 1.123404255319149
frozenset({'Far tours'}) - 0.13617021276595745
frozenset({'DISCOUNT'}) - 0.08297872340425531
frozenset({'tours with tents', 'CATALOG'}) - 0.14468085106382977
frozenset({'CATALOG', 'hiking'}) - 0.14680851063829786
frozenset({'TravelAgency', 'sightseeing tours'}) - 0.08723404255319149
frozenset({'tours with tents', 'TravelAgency'}) - 0.08936170212765958
frozenset({'TravelAgency', 'hiking'}) - 0.11914893617021277
frozenset({'TravelAgency', 'lastminute'}) - 0.13404255319148936
frozenset({'WHOWEARE', 'CATALOG'}) - 0.09148936170212765
frozenset({'Far tours', 'CATALOG'}) - 0.09787234042553192
frozenset({'CATALOG', 'sightseeing tours'}) - 0.125531914893617
frozenset({'tours and holiday comes into hotel', 'CATALOG'}) - 0.10851063829787234
frozenset({'CATALOG', 'light hiking'}) - 0.10851063829787234
frozenset({'CATALOG', 'lastminute'}) - 0.18936170212765957
frozenset({'CATALOG', 'TravelAgency'}) - 0.7021276595744681
frozenset({'CATALOG', 'TravelAgency', 'sightseeing tours'}) - 0.0851063829787234
frozenset({'tours with tents', 'CATALOG', 'TravelAgency'}) - 0.08936170212765958
frozenset({'CATALOG', 'TravelAgency', 'hiking'}) - 0.11914893617021277
frozenset({'CATALOG', 'TravelAgency', 'lastminute'}) - 0.13191489361702127