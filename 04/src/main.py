# Reading csv to a data frame
import pandas as pd
from collections import Counter


def cleanup(df, max_time):
    # Remove too short visits
    print("-----------NO CLEANING SIZE-------------")
    print(df.size)
    print("-----------SIZE AFTER REMOVING TOO SHORT-------------")
    dfclean = df[df["Length_seconds"] > max_time]
    print(dfclean.size)
    return dfclean


def get_conversions(df):
    # main conversions
    main_conv = df[df["PageName"].isin(["APPLICATION", "CATALOG"])]
    print("-----------MAIN CONVERSIONS-------------")
    print(main_conv)

    # micro conversions
    micro_conv = df[df["PageName"].isin(["DISCOUNT", "HOWTOJOIN", "INSURANCE", "WHOWEARE"])]
    print("-----------MICRO CONVERSIONS-------------")
    print(micro_conv)

    return main_conv, micro_conv


def get_dataset(mainconv, clicks, visitors):
    all_main_conv = pd.merge(mainconv, clicks, how="left", on="VisitID")
    print("-----------ALL VISITED PAGES BY USER-------------")
    print(all_main_conv)

    dataset = []
    for visitor in visitors["VisitID"]:
        p = all_main_conv[all_main_conv["VisitID"] == visitor]
        if p.size > 0:
            val = p['PageName_y'].values[:]
            dataset.append(val)
    return dataset


# Apriori algorithm from lecture 05
def frequentItems(transactions, support):
    counter = Counter()
    for trans in transactions:
        counter.update(frozenset([t]) for t in trans)
    return set(item for item in counter if counter[item] / len(transactions) >= support), counter


def generateCandidates(L, k):
    candidates = set()
    for a in L:
        for b in L:
            union = a | b
            if len(union) == k and a != b:
                candidates.add(union)
    return candidates


def filterCandidates(transactions, itemsets, support):
    counter = Counter()
    for trans in transactions:
        subsets = [itemset for itemset in itemsets if itemset.issubset(trans)]
        counter.update(subsets)
    return set(item for item in counter if counter[item] / len(transactions) >= support), counter


def apriori(transactions, support):
    result = list()
    resultc = Counter()
    candidates, counter = frequentItems(transactions, support)
    result += candidates
    resultc += counter
    k = 2
    while candidates:
        candidates = generateCandidates(candidates, k)
        candidates, counter = filterCandidates(transactions, candidates, support)
        result += candidates
        resultc += counter
        k += 1
    resultc = {item: (resultc[item] / len(transactions)) for item in resultc}
    return result, resultc


def loadData():
    clicks = pd.read_csv('clicks.csv')
    search_engine_map = pd.read_csv('search_engine_map.csv')
    visitors = pd.read_csv('visitors.csv')
    return clicks, search_engine_map, visitors


def main():
    clicks, search_engine_map, visitors = loadData()

    print("-----------CLICKS-------------")
    print(clicks.describe())
    print("-----------VISITORS-------------")
    print(visitors.describe())
    print("-----------SEARCH ENGINE MAP-------------")
    print(search_engine_map.describe())

    # Fix TimeOnPage in clicks
    clicks["TimeOnPage"] = pd.cut(clicks["TimeOnPage"], 10)
    # delete LocalID since it gives us 0 information value
    del clicks["LocalID"]

    # Merge clicks, visitors, search_engine_map
    df = pd.merge(pd.merge(clicks, visitors, how="left", on="VisitID"), search_engine_map, how="left", on="Referrer")

    # Remove too short visits
    df = cleanup(df, 20)

    # Find main and micro conversions
    main_conversions, micro_conversions = get_conversions(df)

    # Find dataset for apriori, focus on main_conversions
    dataset = get_dataset(main_conversions, clicks, visitors)

    # Use apriori algorithm from tutorial 05
    frequent_itemsets, supports = apriori(dataset, 0.08)
    print("-----------APRIORI FROM MAIN CONVERSIONS RESULTS-------------")
    for f in frequent_itemsets:
        print("{} - {}".format(f, supports[f]))


if __name__ == "__main__":
    main()
