import csv
import numpy as np
import scipy.sparse
from statistics import mean
from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer
from sklearn.metrics.pairwise import cosine_similarity, euclidean_distances

Q_DIR = "./data/q/"
D_DIR = "./data/d/"
R_DIR = "./data/r/"

D_FILES = 1400
Q_FILES = 225
TOP_N = 15

DATA = list([open(D_DIR + str(d + 1) + ".txt").read() for d in range(D_FILES)])
QUERIES = [open(Q_DIR + str(q + 1) + ".txt").read() for q in range(Q_FILES)]


class ProcessedQuery:
    def __init__(self):
        self.binary_precision = 0
        self.binary_recall = 0
        self.binary_fmeasure = 0
        self.tf_precision = 0
        self.tf_recall = 0
        self.tf_fmeasure = 0
        self.tfidf_precision = 0
        self.tfidf_recall = 0
        self.tfidf_fmeasure = 0

    def construct(self, binary_query, tf_query, tfidf_query, relevant_docs, limit):
        self.binary_precision = calculate_precision(binary_query[:limit], relevant_docs)
        self.binary_recall = calculate_recall(binary_query[:limit], relevant_docs)
        self.binary_fmeasure = calculate_fmeasure(self.binary_precision, self.binary_recall)
        self.tf_precision = calculate_precision(tf_query[:limit], relevant_docs)
        self.tf_recall = calculate_recall(tf_query[:limit], relevant_docs)
        self.tf_fmeasure = calculate_fmeasure(self.tf_precision, self.tf_recall)
        self.tfidf_precision = calculate_precision(tfidf_query[:limit], relevant_docs)
        self.tfidf_recall = calculate_recall(tfidf_query[:limit], relevant_docs)
        self.tfidf_fmeasure = calculate_fmeasure(self.tfidf_precision, self.tfidf_recall)
        return self


def get_relevant_docs(query_id):
    res = []
    with open(R_DIR + str(query_id) + ".txt") as f:
        for line in f.readlines():
            res.append(int(line))
    return res


def get_weighting_result(data_vec, name):
    query_vector = data_vec[data_vec.shape[0] - 1]  # last row is the query
    data_vectors = data_vec[0:data_vec.shape[0] - 1]  # anything but last row is data

    # Sorted euclidean distances from query to rest of data
    euclid_distances = np.array(euclidean_distances(query_vector, data_vectors)[0]).argsort() + 1
    # Sorted cosine similarities from query to rest of data (reversed, higher is better)
    cosine_similarities = np.array(cosine_similarity(query_vector, data_vectors)[0]).argsort()[::-1] + 1

    return euclid_distances, cosine_similarities


def calculate_precision(retrieved_documents, relevant_documents):
    intersections = len(set(retrieved_documents).intersection(set(relevant_documents)))
    return intersections / len(retrieved_documents)


def calculate_recall(retrieved_documents, relevant_documents):
    intersections = len(set(retrieved_documents).intersection(set(relevant_documents)))
    return intersections / len(relevant_documents)


def calculate_fmeasure(precision, recall):
    if precision == 0 and recall == 0:
        return 0
    else:
        return 2 * (precision * recall) / (precision + recall)


def process_query_binary(query):
    data = DATA
    data.append(query)

    # Non zero counts are set to 1
    binary_vectorizer = CountVectorizer(binary=True)
    # Learn the vocabulary dictionary and return term-document matrix.
    vectorized_data = binary_vectorizer.fit_transform(data)

    return get_weighting_result(vectorized_data, "Binary")


def process_query_term_frequency(query):
    data = DATA
    data.append(query)

    vectorizer = CountVectorizer()
    # Learn the vocabulary dictionary and return term-document matrix.
    vectorized_data = vectorizer.fit_transform(data)

    sums = vectorized_data.sum(1)
    normalized_array = vectorized_data.multiply(1 / sums)  # Normalize all rows - divide each row by its sum.

    # Convert to csr_matrix (output class does not support indexing, which we need)
    normalized_array = scipy.sparse.csr_matrix(normalized_array)

    return get_weighting_result(normalized_array, "Term Frequency")


def process_query_tfidf(query):
    data = DATA
    data.append(query)

    tfidf_vectorizer = TfidfVectorizer()
    # prepare matrix
    tfidf_matrix = tfidf_vectorizer.fit_transform(data)

    return get_weighting_result(tfidf_matrix, "TF-IDF")


def main():
    queries = QUERIES
    euclidean = []
    cosine = []

    for i, query in enumerate(queries, 1):
        binary_euc, binary_cos = process_query_binary(query)
        tf_euc, tf_cos = process_query_term_frequency(query)
        tfidf_euc, tfidf_cos = process_query_tfidf(query)

        relevant_docs = get_relevant_docs(i)

        euclidean.append(
            ProcessedQuery().construct(binary_euc, tf_euc, tfidf_euc, relevant_docs, TOP_N))

        cosine.append(
            ProcessedQuery().construct(binary_cos, tf_cos, tfidf_cos, relevant_docs, TOP_N))

    with open('output.csv', mode='w') as f:
        writer = csv.writer(f, delimiter=';')

        columns = ['Query #',
                   'Binary representation euclidean: Precision', 'Binary representation cosine: Precision',
                   'Binary representation euclidean: Recall', 'Binary representation cosine: Recall',
                   'Binary representation euclidean: F-measure', 'Binary representation cosine: F-measure',

                   'Term Frequency euclidean: Precision', 'Term Frequency cosine: Precision',
                   'Term Frequency euclidean: Recall', 'Term Frequency cosine: Recall',
                   'Term Frequency euclidean: F-measure', 'Term Frequency cosine: F-measure',

                   'TF-IDF euclidean: Precision', 'TF-IDF cosine: Precision', 'TF-IDF euclidean: Recall',
                   'TF-IDF cosine: Recall', 'TF-IDF euclidean: F-measure', 'TF-IDF cosine: F-measure']
        writer.writerow(columns)

        for i in range(len(queries)):
            e_res = euclidean[i]
            c_res = cosine[i]

            # File with name 1.txt has index 0 therefore i + 1 needed
            results = [i + 1,
                       e_res.binary_precision, c_res.binary_precision, e_res.binary_recall,
                       c_res.binary_recall, e_res.binary_fmeasure, c_res.binary_fmeasure,

                       e_res.tf_precision, c_res.tf_precision, e_res.tf_recall,
                       c_res.tf_recall, e_res.tf_fmeasure, c_res.tf_fmeasure,

                       e_res.tfidf_precision, c_res.tfidf_precision, e_res.tfidf_recall,
                       c_res.tfidf_recall, e_res.tfidf_fmeasure, c_res.tfidf_fmeasure]
            writer.writerow(results)

            print(
                """
                            Query_{query_num}
                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                Binary
                    - Euclidean:
                        - Precision: {binary_euclidean_precision}
                        - Recall: {binary_euclidean_recall}
                        - F-Measure: {binary_euclidean_fmeasure}
                    - Cosine:
                        - Precision: {binary_cosine_precision}
                        - Recall: {binary_cosine_recall}
                        - F-Measure: {binary_cosine_fmeasure}
                        
                Term frequency
                    - Euclidean:
                        - Precision: {tf_euclidean_precision}
                        - Recall: {tf_euclidean_recall}
                        - F-Measure: {tf_euclidean_fmeasure}
                    - Cosine:
                        - Precision: {tf_cosine_precision}
                        - Recall: {tf_cosine_recall}
                        - F-Measure: {tf_cosine_fmeasure}

                TF-IDF
                    - Euclidean:
                        - Precision: {tfidf_euclidean_precision}
                        - Recall: {tfidf_euclidean_recall}
                        - F-Measure: {tfidf_euclidean_fmeasure}
                    - Cosine:
                        - Precision: {tfidf_cosine_precision}
                        - Recall: {tfidf_cosine_recall}
                        - F-Measure: {tfidf_cosine_fmeasure}
                """.format(
                    query_num=i,

                    binary_euclidean_precision=e_res.binary_precision, binary_euclidean_recall=e_res.binary_recall,
                    binary_euclidean_fmeasure=e_res.binary_fmeasure,

                    binary_cosine_precision=c_res.binary_precision, binary_cosine_recall=c_res.binary_recall,
                    binary_cosine_fmeasure=c_res.binary_fmeasure,

                    tf_euclidean_precision=e_res.tf_precision, tf_euclidean_recall=e_res.tf_recall,
                    tf_euclidean_fmeasure=e_res.tf_fmeasure,

                    tf_cosine_precision=c_res.tf_precision, tf_cosine_recall=c_res.tf_recall,
                    tf_cosine_fmeasure=c_res.tf_fmeasure,

                    tfidf_euclidean_precision=e_res.tfidf_precision, tfidf_euclidean_recall=e_res.tfidf_recall,
                    tfidf_euclidean_fmeasure=e_res.tfidf_fmeasure,

                    tfidf_cosine_precision=c_res.tfidf_precision, tfidf_cosine_recall=c_res.tfidf_recall,
                    tfidf_cosine_fmeasure=c_res.tfidf_fmeasure,

                )
            )

        print(
            """
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                            AVERAGE
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                Binary
                    - Euclidean:
                        - Precision: {binary_euclidean_precision}
                        - Recall: {binary_euclidean_recall}
                        - F-Measure: {binary_euclidean_fmeasure}
                    - Cosine:
                        - Precision: {binary_cosine_precision}
                        - Recall: {binary_cosine_recall}
                        - F-Measure: {binary_cosine_fmeasure}
                        
                Term frequency
                    - Euclidean:
                        - Precision: {tf_euclidean_precision}
                        - Recall: {tf_euclidean_recall}
                        - F-Measure: {tf_euclidean_fmeasure}
                    - Cosine:
                        - Precision: {tf_cosine_precision}
                        - Recall: {tf_cosine_recall}
                        - F-Measure: {tf_cosine_fmeasure}

                TF-IDF
                    - Euclidean:
                        - Precision: {tfidf_euclidean_precision}
                        - Recall: {tfidf_euclidean_recall}
                        - F-Measure: {tfidf_euclidean_fmeasure}
                    - Cosine:
                        - Precision: {tfidf_cosine_precision}
                        - Recall: {tfidf_cosine_recall}
                        - F-Measure: {tfidf_cosine_fmeasure}
            """.format(
                binary_euclidean_precision=mean([res.binary_precision for res in euclidean]),
                binary_euclidean_recall=mean([res.binary_recall for res in euclidean]),
                binary_euclidean_fmeasure=mean([res.binary_fmeasure for res in euclidean]),

                binary_cosine_precision=mean([res.binary_precision for res in cosine]),
                binary_cosine_recall=mean([res.binary_recall for res in cosine]),
                binary_cosine_fmeasure=mean([res.binary_fmeasure for res in cosine]),

                tf_euclidean_precision=mean([res.tf_precision for res in euclidean]),
                tf_euclidean_recall=mean([res.tf_recall for res in euclidean]),
                tf_euclidean_fmeasure=mean([res.tf_fmeasure for res in euclidean]),

                tf_cosine_precision=mean([res.tf_precision for res in cosine]),
                tf_cosine_recall=mean([res.tf_recall for res in cosine]),
                tf_cosine_fmeasure=mean([res.tf_fmeasure for res in cosine]),

                tfidf_euclidean_precision=mean([res.tfidf_precision for res in euclidean]),
                tfidf_euclidean_recall=mean([res.tfidf_recall for res in euclidean]),
                tfidf_euclidean_fmeasure=mean([res.tfidf_fmeasure for res in euclidean]),

                tfidf_cosine_precision=mean([res.tfidf_precision for res in cosine]),
                tfidf_cosine_recall=mean([res.tfidf_recall for res in cosine]),
                tfidf_cosine_fmeasure=mean([res.tfidf_fmeasure for res in cosine]),

            )
        )

        average = [
            "AVERAGE",
            mean([res.binary_precision for res in euclidean]), mean([res.binary_recall for res in euclidean]),
            mean([res.binary_fmeasure for res in euclidean]),

            mean([res.binary_precision for res in cosine]), mean([res.binary_recall for res in cosine]),
            mean([res.binary_fmeasure for res in cosine]),

            mean([res.tf_precision for res in euclidean]), mean([res.tf_recall for res in euclidean]),
            mean([res.tf_fmeasure for res in euclidean]),

            mean([res.tf_precision for res in cosine]), mean([res.tf_recall for res in cosine]),
            mean([res.tf_fmeasure for res in cosine]),

            mean([res.tfidf_precision for res in euclidean]), mean([res.tfidf_recall for res in euclidean]),
            mean([res.tfidf_fmeasure for res in euclidean]),

            mean([res.tfidf_precision for res in cosine]), mean([res.tfidf_recall for res in cosine]),
            mean([res.tfidf_fmeasure for res in cosine])
        ]
        writer.writerow(average)


if __name__ == "__main__":
    main()
